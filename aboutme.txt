I'm motivated to join this bootcamp because becoming a web developer/software engineer entails more career opportunities. My experience here can also be used for businesses that i want to establish in the future.

I am a mechanical engineer by profession, I fix things. Though I have little knowledge about programming, I am confident I can do it because I'm naturally a problem solver.